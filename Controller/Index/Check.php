<?php

namespace Contact\Info\Controller\Index;

use Magento\Framework\View\Result\LayoutFactory;

class Check extends \Magento\Framework\App\Action\Action
{
    protected $layoutFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        LayoutFactory $layoutFactory
    )
    {
        $this->layoutFactory = $layoutFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $resultLayout = $this->layoutFactory->create();
        return $resultLayout;
    }
}