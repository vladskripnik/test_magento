<?php

namespace Contact\Info\Controller\Index;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Contact\Model\ConfigInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Contact\Info\Model\ItemFactory;
use Magento\Catalog\Model\ProductRepository;

class Post extends \Magento\Contact\Controller\Index implements HttpPostActionInterface
{

    private $dataPersistor;

    private $context;

    protected $itemFactory;

    protected $productRepository;

    protected $productSku;

    public function __construct(
        Context $context,
        ConfigInterface $contactsConfig,
        DataPersistorInterface $dataPersistor,
        ItemFactory $itemFactory,
        ProductRepository $productRepository
    )
    {
        parent::__construct($context, $contactsConfig);
        $this->context = $context;
        $this->dataPersistor = $dataPersistor;
        $this->itemFactory = $itemFactory;
        $this->productRepository = $productRepository;
    }

    public function execute()
    {
        $requestForm = $this->getRequest()->getParams();
        $form = $this->itemFactory->create();
        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }
        try {
            $form->setData($requestForm);
            $form->save();
            $this->messageManager->addSuccessMessage(
                __('Thanks for contacting us with your comments and questions.')
            );
            $this->dataPersistor->clear('contact_us');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('contact_us', $this->getRequest()->getParams());
        }
        return $this->resultRedirectFactory->create()->setPath('contact/');
    }
}
