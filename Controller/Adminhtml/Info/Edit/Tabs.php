<?php

namespace Contact\Info\Block\Adminhtml\Info\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

class Tabs extends WidgetTabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('post_edit_tabs');
        $this->setDestElementId('info_form');
        $this->setTitle(__('News Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab(
            'post',
            [
                'label' => __('Contact Us'),
                'title' => __('Contact Us'),
                'content' => $this->getLayout()->createBlock(
                    \Contact\Info\Block\Adminhtml\Info\Edit\Tab\Info::class
                )->toHtml(),
                'active' => true
            ]
        );
        return parent::_beforeToHtml();
    }
}