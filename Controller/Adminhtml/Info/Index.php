<?php

namespace Contact\Info\Controller\Adminhtml\Info;

use Contact\Info\Model\ItemFactory;
use Magento\Framework\Registry;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;

    protected $itemFactory;

    protected $coreRegistry;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        ItemFactory $itemFactory,
        Registry $coreRegistry
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->itemFactory = $itemFactory;
        $this->coreRegistry = $coreRegistry;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Contact_Info::contact');
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }


}