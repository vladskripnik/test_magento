<?php

namespace Contact\Info\Controller\Adminhtml\Info;

use Contact\Info\Controller\Adminhtml\Info\Index;

class View extends Index
{
    public function execute()
    {
        $formId = $this->getRequest()->getParam('id');
        $model = $this->itemFactory->create();
        if ($formId) {
            $model->load($formId);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This form no longer exists.'));
                $this->_redirect('*/*/');
            }
        }
        $this->coreRegistry->register('contactinfo_post', $model);
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Information about form'));
        return $resultPage;
    }
}
