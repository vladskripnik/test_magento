<?php

namespace Contact\Info\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('contact_form_example')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Contact Form ID'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'Name'
        )->addColumn(
            'email',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'Email'
        )->addColumn(
            'telephone',
            Table::TYPE_TEXT,
            15,
            ['nullable' => false, 'default' => ''],
            'Phone Number'
        )->addColumn(
            'comment',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'Comment'
        )->setComment(
            "Greeting Comment table"
        );
        $setup->getConnection()->createTable($table);
        $setup->endSetup();
    }
}