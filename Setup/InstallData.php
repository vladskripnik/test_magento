<?php

namespace Contact\Info\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $setup->getConnection()->insert(
            $setup->getTable('contact_form_example'),
            [
                'name' => 'test',
                'email' => 'test@gmail.com',
                'telephone' => '+380997559225',
                'comment' => 'Have a nice day'
            ]
        );
        $setup->endSetup();
    }
}