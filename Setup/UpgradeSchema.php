<?php

namespace Contact\Info\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            $setup->getConnection()->addColumn(

                $setup->getTable('contact_form_example'),
                'product_id',
                [
                    'type' => Table::TYPE_INTEGER,
                    'length' => 8,
                    'nullable' => false,
                    'comment' => 'Product ID'
                ]

            );
        }

        $setup->endSetup();
    }
}