<?php

namespace Contact\Info\Block\Adminhtml;

class Info extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_info';
        $this->_blockGroup = 'Contact_Info';
        parent::_construct();
        $this->buttonList->remove('add');
    }
}
