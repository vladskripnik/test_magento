<?php

namespace Contact\Info\Block\Adminhtml\Info\Edit\Tab;

use Contact\Info\Block\Adminhtml\ProductForm;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;
use Mastering\SampleModule\Model\System\Config\Status;

class Info extends Generic implements TabInterface
{

    protected $_wysiwygConfig;

    protected $_newsStatus;

    protected $backendHelper;

    protected $productForm;

    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        Status $newsStatus,
        \Magento\Backend\Helper\Data $backendHelper,
        ProductForm $productForm,
        array $data = []
    )
    {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_newsStatus = $newsStatus;
        $this->backendHelper = $backendHelper;
        $this->productForm = $productForm;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {
        $model = $this->checkProduct();
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('post_');
        $form->setFieldNameSuffix('post');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General')]
        );
        if ($model->getId()) {
            $fieldset->addField(
                'id',
                'hidden',
                [
                    'name' => 'id',
                    'label' => __('ID'),
                ]
            );
        };
        $fieldset->addField(
            'name',
            'label',
            [
                'name' => 'name',
                'label' => __('Name'),
            ]
        );
        $fieldset->addField(
            'email',
            'label',
            [
                'name' => 'email',
                'label' => __('Email'),
            ]
        );
        $fieldset->addField(
            'telephone',
            'label',
            [
                'name' => 'telephone',
                'label' => __('Telephone'),
            ]
        );
        $fieldset->addField(
            'comment',
            'label',
            [
                'name' => 'comment',
                'label' => __('Comment'),
            ]
        );
        if ($model->getProductId()) {

            $fieldset->addField(
                'product_name',
                'label',
                [
                    'name' => 'product_name',
                    'label' => __('Product Name'),
                ]
            );
            $fieldset->addField(
                'product_url',
                'link',
                [
                    'name' => 'product_url',
                    'label' => __('Product Url'),
                ]
            );
        }
        $data = $model->getData();
        $form->setValues($data);
        $this->setForm($form);
        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return __('Contact Info');
    }

    public function getTabTitle()
    {
        return __('Contact Info');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getModel()
    {
        return $this->_coreRegistry->registry('contactinfo_post');
    }

    public function checkProduct()
    {
        $model = $this->getModel();
        if ($model->getProductId()) {
            $productAdminUrl = $this->backendHelper->getUrl(
                'catalog/product/edit',
                ['id' => $model->getProductId()]
            );
            $productName = $this->productForm->getProductNameById($model->getProductId());
            $model->addData(['product_url' => $productAdminUrl, 'product_name' => $productName]);
            return $model;
        } else {
            return $model;
        }
    }
}