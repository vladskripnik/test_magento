<?php

namespace Contact\Info\Block\Adminhtml;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Catalog\Model\ProductRepository;

class ProductForm extends AbstractBlock
{

    protected $helperData;

    protected $productRepository;

    public function __construct(
        Context $context,
        ProductRepository $productRepository,
        array $data = []
    )
    {
        $this->productRepository = $productRepository;
        parent::__construct($context, $data);
    }

    public function getOutForm()
    {
        $requestSKU = $this->getRequest()->getParam('comment');
        try {
            $productId = $this->getProductId($requestSKU);
            $productName = $this->getProductNameBySKU($requestSKU);
            $productUrl = $this->getProductBaseUrl($requestSKU);
            $form = '<table>';
            $form .= "<tr><td>" . $productName . "</td>";
            $form .= "<td>" . "<a href=\"$productUrl\">" . $productUrl . "</td></tr>";
            $form .= "<tr><td><input type=\"checkbox\" id=\"product_id\" name=\"product_id\" value=\"$productId\">";
            $form .= "<td>" . 'You mentioned this product(s). Want to attach it to the request?' . "</td>";
            $form .= "</table>";
            return $form;
        } catch (\Exception $e) {
            return false;
        }
    }

    protected function _toHtml()
    {
        return $this->getOutForm();
    }

    public function getProductNameBySKU($productSKU)
    {
        return $this->productRepository->get($productSKU)->getName();
    }

    public function getProductId($productSKU)
    {
        return $this->productRepository->get($productSKU)->getId();
    }

    public function getProductBaseUrl($productSKU)
    {
        return $this->productRepository->get($productSKU)->getProductUrl();
    }

    public function getProductNameById($productId)
    {
        return $this->productRepository->getById($productId)->getName();
    }

}