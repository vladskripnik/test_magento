<?php

namespace Contact\Info\Source\Options;

use Magento\Framework\Data\OptionSourceInterface;

class Value implements OptionSourceInterface
{
    protected $backendHelper;

    protected $collection;

    public function __construct(
        \Magento\Backend\Helper\Data $backendHelper,
        \Contact\Info\Model\ResourceModel\Item\Collection $collection
    )
    {
        $this->collection = $collection;
        $this->backendHelper = $backendHelper;
    }

    public function toOptionArray()
    {
        $model = $this->collection->getData();
        $options = [];
        foreach ($model as $item) {
            $productId = $item['product_id'];
            $productAdminUrl = $this->backendHelper->getUrl(
                'catalog/product/edit',
                ['id' => $productId]
            );
            if ($productId !== '0') {
                $options[$productId] = $productAdminUrl;
            }
        }
        return $options;
    }
}