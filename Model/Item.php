<?php

namespace Contact\Info\Model;

use Magento\Framework\Model\AbstractModel;

class Item extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Contact\Info\Model\ResourceModel\Item::class);
    }
}