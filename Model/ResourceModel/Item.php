<?php

namespace Contact\Info\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Item extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('contact_form_example', 'id');
    }

    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $model)
    {
        if (trim($model->getData('name')) === '') {
            throw new \Exception(__('Enter the Name and try again.'));
        }
        if (trim($model->getData('comment')) === '') {
            throw new \Exception(__('Enter the comment and try again.'));
        }
        if (false === \strpos($model->getData('email'), '@')) {
            throw new \Exception(__('The email address is invalid. Verify the email address and try again.'));
        }
        if (trim($model->getData('hideit')) !== '') {
            throw new \Exception();
        }
    }
}