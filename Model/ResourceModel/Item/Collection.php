<?php

namespace Contact\Info\Model\ResourceModel\Item;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Contact\Info\Model\Item;
use Contact\Info\Model\ResourceModel\Item as ItemResource;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(
            Item::class, ItemResource::class);
    }

    protected function _renderFiltersBefore()
    {
        $this->getSelect()->joinLeft(
            'catalog_product_entity_varchar as so',
            'main_table.product_id = so.entity_id and so.attribute_id = 73',
            ['value']
        );
        parent::_renderFiltersBefore();
    }
}