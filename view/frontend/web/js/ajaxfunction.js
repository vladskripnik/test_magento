define([
    'jquery',
], function ($) {
    return function (config, element) {
        $(element).change(function () {
            $('.field-recaptcha').load(config.url, {'comment': $(element).val()});
        });
    }
});